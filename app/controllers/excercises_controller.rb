class ExcercisesController < ApplicationController
  def create
    @workout = Workout.find(params[:workout_id])
    @excercise = @workout.excercises.create(params[:excercise].permit(:name, :sets, :reps))
    redirect_to workout_path(@workout)
  end

end
