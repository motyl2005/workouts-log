Rails.application.routes.draw do
  
  resources :workouts do
    resources :excercises
  end
  root "workouts#index"
end
